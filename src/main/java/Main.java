import javax.persistence.EntityManagerFactory;
import javax.persistence.*;
import entities.Student;

public class Main {
    public static void main(String[] args) {
        EntityManagerFactory emf = Persistence.createEntityManagerFactory("School");
        EntityManager em = emf.createEntityManager();           // заказывает менеджера из фабрики
        em.getTransaction().begin();

        // Create
        Student s = new Student("Test3", "Test3", 2000, 11f); // конструктор ID НЕ ПРИНИМАЕТ! JPA сам даст ему ID
        em.persist( s );    // отправляет в базу через манаджера
        em.getTransaction().commit();

        //Read
        Student student = em.find(Student.class,26);
        System.out.println("\n Student with ID=26: " + student + "\n");

        //Update
        Student student_update = em.find(Student.class,31);
        em.getTransaction().begin();
        student_update.setFirstname("Tolea");
        student_update.setLastname("Petrov");
        em.getTransaction().commit();

        //Delete
        Student student_delete = em.find(Student.class,34);
        em.getTransaction().begin();
        em.remove(student_delete);
        em.getTransaction().commit();

        em.close();
        emf.close();
    }
}
